# Service Answer

Yet another mean to standardize a service answer with error management.

## How to Install

#### using [Composer](http://getcomposer.org/)

Create a composer.json file in your project root:

```json
{
    "require": {
        "voilab/serviceanswer": "dev-master"
    }
}
```

Then run the following composer command:

```bash
$ php composer.phar install
```

#### Usage

...see the code...

## Authors

[Joel Poulin](http://www.voilab.org)

## License

MIT Public License
